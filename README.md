# Quiz code test

Quiz test made for both backend and frontend.

## Reasonings for architectural choices

Most of my code is written in Javascript + TypeScript as I feel the most comfortable with this language because of long experience in it both in frontend and backend. Nodejs is the next logical reason as I have been using it quite extensively in the past as well so it makes it much easier for prototyping everything I do as quick as possible.

Because the test case is quite huge I didn't try to experiment with new technologies as it would take more time for me to get it done and even with 4-5 days time, it would not be enough for me to make a decent project backplate.

One of the frameworks that I used here but haven't worked with before is VueJS, even though I've heard of it and tried a bit on my free time I do not have a lot of experience with it. It took a bit of a learning on how to put everything together but in the end I think it turned out better than expected. Some of the issues I was having was to make the frontend wait for the endpoint to retrieve information. It's visible when starting a quiz when there are no answers yet present. I've made some decisions on pre-fetching certain parts to make it feel more fluid. Additionally I feel that my code structure is a bit messy, but I believe that it's because I haven't really had experience with the VueJS but I know that quickly I'd get a hang of it because of it's similarities with React and my experience with React framework.

Another interesting technology I used was Docker. Even though it was not specified I used Docker to make it easier to set the project up on any machine without the need to install anything on locally instead of Docker itself.

In total this project took me less than 3 days to finish even though I expected it to be done with in 4-5 days because of settling down.

Some parts that I would like to improve:

    - Code structure in VueJS.
    - Prefetching questions/answers in time to not create cumulative layout shifts.
    - Improve on design, for example, styled dropdown, better transition/hover effects.
    - Make progress bar more stylistic.

## Setup guide

1. Install [Docker](https://docs.docker.com/v17.09/engine/installation/#desktop) (Note: it's possible to run it without Docker. You can skip 1,2 and 4th step in that case and do step 4.1 instead)
2. Install [docker-compose](https://docs.docker.com/compose/install/)
3. Clone this repository
4. Under cloned repositories directory run
    ```
        docker-compose up -d --build
    ```
    4.1. Install mongoDB on your machine, make sure that it's running
    
    4.2. In app.ts replace ```mongodb://mongo:27017/mongo``` with ```mongodb://127.0.0.1:27017/mongo```
    
    4.3. To run server do ```cd server && npm install && npm run serve```
5. To run frontend
    ```
        cd client
        npm install
        npm run dev
    ```
6. Now everything should be up and running!
7. You can access now
    ```
        Frontend under localhost:3000
        Backend under localhost:8080
        MongoDB under localhost:27017
    ```

---

## Available Enpoints

## Get all users : `GET /users`

### Success Response

**Code** : `200 OK`

```json
[
    {
        "_id": "60cca6fdd438ce00208e8dc0",
        "name": "Matt",
        "completedQuizzes": [
            {
                "results": {
                    "correct": 3,
                    "total": 6
                },
                "answers": [
                    14030,
                    57737,
                    262891,
                    141001,
                    3310741,
                    73130063
                ],
                "_id": "60cca705d438ce00208e8dc3",
                "quizId": 141
            }
        ],
        "__v": 1
    },
    {
        "_id": "60cca70ed438ce00208e8dc6",
        "name": "Frank",
        "completedQuizzes": [],
        "__v": 0
    }
]
```

---

## Create a user : `POST /user`

### Example request

```json
{
	"name": "Frank"
}
```
- If user with given name exists then backend will return the existing user object, otherwise it will create new user

### Success Response

**Code** : `200 OK`

```json
{
    "_id": "60cca70ed438ce00208e8dc6",
    "name": "Frank",
    "completedQuizzes": [],
    "__v": 0
}
```

### Error Resonse

**Code** : `400 Bad Request`

If `name` is missing then an error JSON will be sent back with

```json
{
    "error": "Missing parameters!",
    "message": "Make sure that key 'name' is specified"
}
```

---

## Complete a quiz : `POST /completedQuiz`

### Example request

```json
{
    "userId": "60cca6fdd438ce00208e8dc0",
    "quizId": 141,
    "answers": [
        14030,
        57737,
        262891,
        845102,
        1123111,
        9198261
    ],
    "results": {
        "correct":4,
        "total":6
    }
}
```

### Success Response

**Code** : `200 OK`

```json
{
    "_id": "60cca6fdd438ce00208e8dc0",
    "name": "Matt",
    "completedQuizzes":[
        {
            "answers": [
                14030,
                57737,
                262891,
                845102,
                1123111,
                9198261
            ],
            "_id": "60cca809d438ce00208e8dcc",
            "quizId": 141,
            "results": {
                "correct": 4,
                "total": 6
            }
        }
    ],
    "__v": 2
}
```

### Error Resonse

**Code** : `400 Bad Request`

If `userId` is missing then an error JSON will be sent back with

```json
{
    "error": "Missing parameters!",
    "message": "Make sure that key 'userId' is specified"
}
```

If `quizId` is in incorrect format then an error JSON will be sent back with

```json
{
    "error": "Missing parameters!",
    "message": "Make sure that key 'quizId' is specified"
}
```

If `answers` is not found then an error JSON will be sent back with

```json
{
    "error": "Missing parameters!",
    "message": "Make sure that key 'answers' is specified"
}
```

If `results` is not found then an error JSON will be sent back with

```json
{
    "error": "Missing parameters!",
    "message": "Make sure that key 'results' is specified"
}
```

---
## Quiz was made using following technologies

- [Node.js](https://nodejs.org/en/) - Server-side scripting
- [MongoDB](https://www.mongodb.com/) - General purpose database
- [Docker / Docker-compose](https://www.docker.com/) - Containerized development
- [TypeScript](https://www.typescriptlang.org/) - Programming language
- [Vite](https://vitejs.dev/) - Build tool for serving frontend
- [VueJS](https://vuejs.org/) - Frontend framework