const baseURL = 'http://localhost:8080';

const postOptions = (body) => ({
    method: 'POST',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
});

export const postUser = (name) => fetch(`${baseURL}/user`, postOptions({ name })).then((res) => res.json());
export const postCompletedQuiz = (userId, quizId, answers, results) => fetch(`${baseURL}/completedQuiz`, postOptions({ userId, quizId, answers, results}));