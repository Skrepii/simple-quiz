const baseURL = 'https://printful.com/test-quiz.php';

export const fetchQuizOptions = () => fetch(`${baseURL}?action=quizzes`).then((res) => res.json());
export const fetchQuestions = (quizId) => fetch(`${baseURL}?action=questions&quizId=${quizId}`).then((res) => res.json());
export const fetchAnswers = (quizId, questionId) => fetch(`${baseURL}?action=answers&quizId=${quizId}&questionId=${questionId}`).then((res) => res.json());
export const fetchResults = (quizId, answers) => {
    const formattedAnswers = answers.map((answerId) => `&answers[]=${answerId}`).join('');
    return fetch(`${baseURL}?action=submit&quizId=${quizId}${formattedAnswers}`).then((res) => res.json());
};