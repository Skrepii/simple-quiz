import { createStore } from 'vuex';

export const store = createStore({
    state: {
        user: null,
        quizId: null,
        answers: [],
        questions: {}
    },
    mutations: {
        setUser(state, user) {
            if (user !== null && user !== '') {
                state.user = user;
            }
        },
        setCurrentQuiz(state, quizId) {
            state.quizId = quizId;
        },
        setQuestions(state, questions) {
            state.questions = questions;
        },
        setAnswers(state, answers) {
            state.answers = answers;
        }
    },
    actions: {
        setUser({ commit }, user) {
            commit('setUser', user);
        },
        setCurrentQuiz({ commit }, quizId) {
            commit('setCurrentQuiz', quizId);
        },
        setQuestions({ commit, state }, { quizId, questions }) {
            commit('setQuestions', { ...state.quiz, [quizId]: questions })
        },
        addAnswer({ commit, state }, answerId) {
            commit('setAnswers', [ ...state.answers, answerId ]);
        },
        clearAnswers({ commit }) {
            commit('setAnswers', []);
        }
    },
    getters: {

    }
});