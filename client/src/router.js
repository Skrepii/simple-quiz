import { createRouter, createWebHistory } from 'vue-router';

import Login from './components/Login.vue';
import Quiz from './components/Quiz.vue';
import Results from './components/Results.vue';

export const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', component: Login, meta: { guest: true } },
        { path: '/quiz/:id', component: Quiz, props: true, meta: { needLogin: true } },
        { path: '/results', component: Results, props: true, meta: { needLogin: true } }
    ]
});