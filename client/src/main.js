import { createApp } from 'vue';

import { store } from './store';
import { router } from './router';

import App from './App.vue';

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.guest)) {
        next();
    } else if (to.matched.some(record => record.meta.needLogin) && store.state.user !== null) {
        next();
    } else {
        next({ path: '/' });
    }
});

const app = createApp(App);
app.use(router);
app.use(store);
app.mount('#app');
