import express, { Request, Response, Application } from 'express';
import mongoose from 'mongoose';
import { readFileSync } from 'fs';
import { resolve } from 'path';
import cors from 'cors';

import { User } from './model/user';
import { sessionalize } from './helpers';

export const app: Application = express();
const PORT = process.env.PORT || 8080;
const mongoURL = process.env.MONGOURL || 'mongodb://mongo:27017/mongo';

app.use(cors());
app.use(express.json());

/*
    Gets all users from database
*/
app.get('/users', async (_, res: Response) => {
    try {
        res.json(await User.find({}));
    } catch (err) {
        res.status(500).json({
            error: 'Users not found!',
            message: 'Make sure that database is running'
        });
    }
});

/* 
    Creates new user
    Requires POST JSON body with parameter { name }
    
    Example request:
    localhost:8080/user
    {
        'name': 'Matt'
    }
    - If user was created a user object will be returned
    - If name already exists it will return existing user object
*/
app.post('/user', async (req: Request, res: Response) => {
    const { name } = req.body;
    if (!name) {
        res.status(400).json({
            error: 'Missing parameters!',
            message: 'Make sure that key \'name\' is specified'
        });
        return;
    }

    const user = await User.findOne({ name });
    if (user !== null) {
        res.json(user);
        return;
    }

    try {
        const newUser = await sessionalize(User.createUser, name);
        res.json(newUser);
    } catch (err) {
        res.status(400).json(err);
    }
});

/*
    Saves completed quiz for a user
    Requires POST JSON body with parameter { userId, quizId, answers, results }
    
    Example request:
    localhost:8080/completedQuiz
    {
        userId: "60cc67842e12a548107b4427",
        quizId: 169,
        answers: [
            508313117, 1249723056, 11145937790, 28755353920
        ],
        results: {
            correct: 4,
            total: 5
        }
    }
    - If completed quiz was saved correctly it will return user object with all the data
*/
app.post('/completedQuiz', async (req: Request, res: Response) => {
    const { userId, quizId, answers, results } = req.body;

    if (!userId) {
        res.status(400).json({
            error: 'Missing parameters!',
            message: 'Make sure that key \'userId\' is specified'
        });
        return;
    }
    
    if (!quizId) {
        res.status(400).json({
            error: 'Missing parameters!',
            message: 'Make sure that key \'quizId\' is specified'
        });
        return;
    }

    if (!answers) {
        res.status(400).json({
            error: 'Missing parameters!',
            message: 'Make sure that key \'answers\' is specified'
        });
        return;
    }

    if (!results) {
        res.status(400).json({
            error: 'Missing parameters!',
            message: 'Make sure that key \'results\' is specified'
        });
        return;
    }

    try {
        const user = await User.findById(userId);

        if (user === null) {
            res.status(400).json({
                error: 'User not found!',
                message: 'Make sure that key \'userId\' is correct'
            });
            return;
        }

        res.json(await user.finishQuiz({ quizId, answers, results }));
    } catch (err) {
        console.log(err.response.body);
    }
});

export let server = app.listen(PORT, async () => {
    try {
        await mongoose.connect(mongoURL, { useNewUrlParser: true, useUnifiedTopology: true });
        if (process.env.DROP_USERS === 'true') {
            try {
                await User.collection.drop();
            } catch (err) {
                console.error('Failed to drop user collection\n', err);
            }
        }
    } catch (err) {
        console.error('Failed to connect to mongoDB server\n', err);
    }
    console.log('Listening on port:', PORT);
});