import { Document, Schema, model, Model } from 'mongoose';

export interface IResult {
    correct: number;
    total: number;
}

export interface ICompletedQuiz {
    quizId: number;
    answers: number[];
    results: IResult;
}

export interface IUser extends Document {
    name: string;
    completedQuizzes: ICompletedQuiz[];

    finishQuiz(completedQuiz: ICompletedQuiz): Promise<void>;
}

export interface IUserModel extends Model<IUser> {
    createUser(name: string): Promise<IUser>;
}

const UserSchema: Schema<IUser, IUserModel> = new Schema({
    name: { type: String, required: true },
    completedQuizzes: [
        { 
            quizId: Number,
            answers: [ Number ],
            results: {
                correct: Number,
                total: Number
            }
        }
    ]
});

UserSchema.methods.finishQuiz = async function (completedQuiz: ICompletedQuiz): Promise<any> {
    return new Promise(async (resolve, reject) => {
        this.completedQuizzes.push(completedQuiz);
        try {
            resolve(await this.save());
        } catch (err) {
            reject({ message: 'Failed to save finished quiz', err});
        }
    });
};

UserSchema.statics.createUser = async function (name: string): Promise<IUser> {
    return new Promise(async (resolve, reject) => {
        let user: IUser = new User({ name, completedQuizzes: [] });
        try {
            resolve(await user.save());
        } catch (err) {
            reject({ message: 'Failed to save user', err });
        }
    });
};

export const User = model<IUser, IUserModel>('User', UserSchema);